FROM golang:alpine AS builder
MAINTAINER Michael Okoko<hi@mchl.xyz>

COPY go.mod go.sum /go/src/gitlab.com/idoko/hastener/
WORKDIR /go/src/gitlab.com/idoko/hastener
RUN go mod download
COPY . /go/src/gitlab.com/idoko/hastener
RUN CGO_ENABLED=0 GOOS=linux go build -a --installsuffix cgo -o build/hastener ./cmd/hastener

FROM alpine
RUN apk add --no-cache ca-certificates && update-ca-certificates
COPY --from=builder /go/src/gitlab.com/idoko/hastener/build/hastener /usr/bin/hastener

ENTRYPOINT ["/usr/bin/hastener"]