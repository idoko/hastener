package model

import (
	"time"
)

type Status string

const (
	StatusDone    = Status("done")
	StatusFailed  = Status("failed")
	StatusQueued  = Status("queued")
	StatusRunning = Status("running")
)

type Job struct {
	ObjectId      int64     `json:"object_id,omitempty"`
	JobId         string    `json:"job_id,omitempty"`
	StartTime     time.Time `json:"start_time,omitempty"`
	EndTime       time.Time `json:"end_time,omitempty"`
	Status        Status    `json:"status,omitempty"`
	StatusMessage string    `json:"status_message,omitempty"`
	CreatedAt     time.Time `json:"created_at,omitempty"`
}
