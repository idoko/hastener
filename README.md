## Hastener
A "job queue" REST API with background job processing, job completion alerts, and lots of love.
It's written in Go and uses PostgreSQL (as both a data store and a queue store).
## Requirements
Using the project assumes you have the following installed:
- make
- docker
- docker-compose
- psql (PostgreSQL command line client to setup the database schema)

### Environment setup
- Copy the `.env.example` file to `.env` to make it ready for use.

### Database setup
To setup the database schema, start the PostgreSQL docker service by running:
```
make run-pg
```
Next, enter into the postgres shell by running the commands below
```bash
$ PG_CONTAINER_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' hastener_postgres_1) 
$ psql -h $PG_CONTAINER_IP -p 5432 -U hastener -d hastener_db 
```
The database password is `hastener_secrets` (or the value of the `POSTGRES_PASSWORD` variable in the `.env` file).
### Go...!
You can now see the project in action by running
```bash
$ make run
```
This will build the `api` docker image and run it (alongisde a PostgreSQL) image. 
## Milestones
- [x] User performs `POST` request and receives an Object ID
- [x] Background job processing.
- [x] Notify of job completion.
- [x] Automatically fail job after timeout.
- [x] Persist job execution details
- [x] User performs `GET` request to get job info
- [ ] Heal jobs and resume them after crashes.
## Limitations
At the moment, crashes/restarts will result in failed jobs. It chose to do this instead of restarting
the jobs as it currently has no way of tracking how much progress has been made with the job.
## Tests
Tests showing worker behaviour are in" `worker/worker_test.go`. They can be run with:
```bash
$ make test
```