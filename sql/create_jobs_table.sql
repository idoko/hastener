CREATE TABLE IF NOT EXISTS jobs (
    job_id VARCHAR(100) PRIMARY KEY,
    object_id INT NOT NULL,
    start_time TIMESTAMP DEFAULT NULL,
    end_time TIMESTAMP DEFAULT NULL,
    status VARCHAR(10) DEFAULT 'queued',
    status_message VARCHAR(150),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);