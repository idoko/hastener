module gitlab.com/idoko/hastener

go 1.16

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/google/uuid v1.3.0
	github.com/lib/pq v1.10.2
	github.com/rs/zerolog v1.23.0
	github.com/stretchr/testify v1.5.1 // indirect
	golang.org/x/sys v0.0.0-20210423082822-04245dca01da // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
