package db

import (
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	_ "github.com/lib/pq"
	"github.com/rs/zerolog"
	"gitlab.com/idoko/hastener/model"
	"time"
)

type PgDB struct {
	Conn                *sql.DB
	Logger              zerolog.Logger
	DuplicationInterval time.Duration
}

var ErrNotFound = fmt.Errorf("no matching record found")

type Config struct {
	Host     string
	Port     int
	Username string
	Password string
	DbName   string
	Logger   zerolog.Logger
}

func InitPg(cfg Config) (PgDB, error) {
	db := PgDB{}
	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.Host, cfg.Port, cfg.Username, cfg.Password, cfg.DbName)
	conn, err := sql.Open("postgres", dsn)
	if err != nil {
		return db, err
	}

	db.Conn = conn
	db.Logger = cfg.Logger
	err = db.Conn.Ping()
	if err != nil {
		return db, err
	}
	return db, nil
}

func (pg PgDB) SaveJob(job *model.Job) error {
	jobId := uuid.New()
	job.JobId = jobId.String()
	var err error

	query := `INSERT INTO jobs(job_id, object_id) VALUES ($1, $2)`
	_, err = pg.Conn.Exec(query, job.JobId, job.ObjectId)
	if err != nil {
		return err
	}
	return nil
}

func (pg PgDB) GetJob(jobId string) (*model.Job, error) {
	var job model.Job
	query := `SELECT job_id, object_id, status, start_time, end_time, status_message, created_at FROM jobs WHERE job_id = $1 LIMIT 1`
	row := pg.Conn.QueryRow(query, jobId)
	err := row.Scan(&job.JobId, &job.ObjectId, &job.Status, &job.StartTime, &job.EndTime, &job.StatusMessage, &job.CreatedAt);
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrNotFound
		}
		return nil, err
	}
	return &job, nil
}

func (pg PgDB) NextJob() (*model.Job, error) {
	var err error
	job := model.Job{}

	query := `SELECT job_id, object_id, status FROM jobs 
				WHERE status = $1 ORDER BY created_at FOR UPDATE SKIP LOCKED
				LIMIT 1`
	row := pg.Conn.QueryRow(query, model.StatusQueued)
	err = row.Scan(&job.JobId, &job.ObjectId, &job.Status)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	return &job, nil
}

func (pg PgDB) ObjectInQueue(objectID int64) bool {
	var job model.Job
	query := `SELECT job_id, object_id, status, created_at FROM jobs WHERE object_id=$1 AND status IN ($2, $3) LIMIT 1`
	row := pg.Conn.QueryRow(query, objectID, model.StatusQueued, model.StatusRunning)
	err := row.Scan(&job.JobId, &job.ObjectId, &job.Status, &job.CreatedAt)
	if err != nil {
		if err == sql.ErrNoRows {
			return false
		}
		pg.Logger.Err(err).Msg("failed to check jobs in queue, will block subsequent jobs")
		return true
	}
	if time.Since(job.CreatedAt) < (5 * time.Minute) {
		return true
	} else {
		// the job is stuck, fail and skip over
		job.Status = model.StatusFailed
		job.StatusMessage = "failing job after being stuck"
		_ = pg.FailJob(job)
	}
	return false
}

func (pg PgDB) StartJob(job model.Job) (err error) {
	query := "UPDATE jobs SET status=$1, start_time=$2 WHERE job_id=$3"
	_, err = pg.Conn.Exec(query, model.StatusRunning, job.StartTime, job.JobId)
	if err != nil {
		return err
	}
	return nil
}

func (pg PgDB) FailJob(job model.Job) (err error) {
	query := "UPDATE jobs SET status=$1, status_message=$2, end_time=$3 WHERE job_id=$4"
	_, err = pg.Conn.Exec(query, model.StatusFailed, job.StatusMessage, job.EndTime, job.JobId)
	if err != nil {
		return err
	}
	return nil
}

func (pg PgDB) MarkJobAsDone(job model.Job) (err error) {
	query := "UPDATE jobs SET status=$1, status_message=$2, end_time=$3 WHERE job_id=$4"
	_, err = pg.Conn.Exec(query, model.StatusDone, job.StatusMessage, job.EndTime, job.JobId)
	if err != nil {
		return err
	}
	return nil
}
