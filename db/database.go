package db

import (
	"gitlab.com/idoko/hastener/model"
)

type StoreDatabase interface {
	SaveJob(job *model.Job) error
	GetJob(jobId string) (*model.Job, error)
	ObjectInQueue(objectID int64) bool
}

type WorkerDatabase interface {
	NextJob() (job *model.Job, err error)
	StartJob(job model.Job) (err error)
	FailJob(job model.Job) (err error)
	MarkJobAsDone(job model.Job) (err error)
}
