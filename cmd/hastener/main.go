package main

import (
	"bytes"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"gitlab.com/idoko/hastener/api"
	"gitlab.com/idoko/hastener/db"
	"gitlab.com/idoko/hastener/model"
	"gitlab.com/idoko/hastener/worker"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"
)

func main() {
	var dbPort int
	var err error
	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()

	var jobTimeout int
	if jobTimeout, err = strconv.Atoi(os.Getenv("job_timeout")); err != nil {
		jobTimeout = 5
	}

	port := os.Getenv("POSTGRES_PORT")
	if dbPort, err = strconv.Atoi(port); err != nil {
		logger.Err(err).Msg("failed to parse database port")
		os.Exit(1)
	}
	dbConfig := db.Config{
		Host:     os.Getenv("POSTGRES_HOST"),
		Port:     dbPort,
		Username: os.Getenv("POSTGRES_USER"),
		Password: os.Getenv("POSTGRES_PASSWORD"),
		DbName:   os.Getenv("POSTGRES_DB"),
		Logger:   logger,
	}
	logger.Info().Interface("config", &dbConfig).Msg("config:")
	dbInstance, err := db.InitPg(dbConfig)
	if err != nil {
		logger.Err(err).Msg("Connection failed")
		os.Exit(1)
	}
	logger.Info().Msg("StoreDatabase connection established")

	stopCh := setupSignalHandler()

	jobHandler := func(ctx context.Context, job *model.Job) (err error) {
		rand.Seed(time.Now().UnixNano())
		min := 50
		max := 100
		slumberTime := rand.Intn((max - min + 1) + min)
		logger.Info().Msgf("Processing Job: %s with slumber time: %d seconds", job.JobId, slumberTime)
		select {
		case <-time.After(time.Duration(slumberTime) * time.Second):
			return nil
		case <-ctx.Done():
			return ctx.Err()
		}
	}

	srv := api.NewApi(dbInstance, logger)
	router := gin.Default()
	rg := router.Group("/v1")
	srv.Register(rg)

	wk := worker.NewWorker(dbInstance, logger, time.Duration(jobTimeout)*time.Minute, jobHandler)
	wk.AttachListener(func(job *model.Job) error {
		url := "http://localhost:8080/v1/notify"
		var jsonStr = []byte(fmt.Sprintf(`{"job_id":"%s"`, job.JobId))
		req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()
		logger.Info().Msgf("response Status: %s", resp.Status)
		return nil
	})
	go wk.Run(stopCh)

	router.Run(":8080")
}

func setupSignalHandler() <-chan struct{} {
	stop := make(chan struct{})
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		<-c
		close(stop)
		os.Exit(1) // close if a second signal is caught
	}()
	return stop
}
