docker:
	docker build -t idoko/hastener .
run-pg:
	docker-compose up postgres
run-api:
	docker-compose build api && docker up api
test:
	go test ./...
run:
	docker-compose up --build
go-fmt:
	go fmt ./...
build:
	go build -o build/hst ./cmd/hastener
clean:
	rm -rf ./build