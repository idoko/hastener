package api

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"gitlab.com/idoko/hastener/db"
)

type Api struct {
	DB     db.StoreDatabase
	Logger zerolog.Logger
}

func NewApi(store db.StoreDatabase, logger zerolog.Logger) Api {
	return Api{
		DB:     store,
		Logger: logger,
	}
}

func (api *Api) Register(group *gin.RouterGroup) {
	group.GET("/jobs/:job_id", api.GetJob)

	// post over put since we are letting the server generate the job id
	group.POST("/jobs", api.CreateJob)

	// to broadcast job completion/handling - we could for instance send an email in this handler
	group.POST("/notify", api.Notify)
}
