package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/idoko/hastener/db"
	"gitlab.com/idoko/hastener/model"
	"net/http"
)

func (api *Api) CreateJob(c *gin.Context) {
	var job model.Job
	reqBody := struct {
		ObjectID int64 `json:"object_id,string,omitempty"`
	}{}
	if err := c.Bind(&reqBody); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": fmt.Sprintf("invalid object id in request body: %s", err.Error()),
		})
		return
	}

	if api.DB.ObjectInQueue(reqBody.ObjectID) {
		c.JSON(http.StatusTooManyRequests, gin.H{
			"error": fmt.Sprintf("provided object ID (%d) is in queue, please retry later", reqBody.ObjectID),
		})
		return
	}

	job.ObjectId = reqBody.ObjectID
	err := api.DB.SaveJob(&job)
	if err != nil {
		api.Logger.Err(err).Msg("error saving job")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": fmt.Sprintf("error saving job: %s", err.Error()),
		})
	} else {
		res := map[string]string{
			"job_id": job.JobId,
		}
		c.JSON(http.StatusCreated, gin.H{"data": res})
	}
}

func (api *Api) GetJob(c *gin.Context) {
	jobId := c.Param("job_id")
	job, err := api.DB.GetJob(jobId)
	if err != nil {
		if err == db.ErrNotFound {
			c.JSON(http.StatusNotFound, gin.H{
				"error": fmt.Sprintf("could not find job with ID: %s", jobId),
			})
		}
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": fmt.Sprintf("could not fetch job: %s", err.Error()),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"data": job,
	})
}

func (api *Api) Notify(c *gin.Context) {
	//this could be a signal to send an email or a notification
	// to the user that owns the job (which is sent here as part of request body).
	c.JSON(http.StatusOK, gin.H{"success": true})
}
