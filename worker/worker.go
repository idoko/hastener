package worker

import (
	"context"
	"fmt"
	"github.com/rs/zerolog"
	"gitlab.com/idoko/hastener/db"
	"gitlab.com/idoko/hastener/model"
	"time"
)

type HandlerFunc func(ctx context.Context, job *model.Job) (err error)
type ListenerFunc func(job *model.Job) error

type Worker struct {
	DB      db.WorkerDatabase
	Logger  zerolog.Logger
	Handler HandlerFunc
	// Timeout specifies how long jobs should be allowed to run in seconds
	Timeout  time.Duration
	Listener ListenerFunc
}

func NewWorker(workerDb db.WorkerDatabase, logger zerolog.Logger, timeout time.Duration, handlerFunc HandlerFunc) Worker {
	return Worker{
		DB:      workerDb,
		Logger:  logger,
		Handler: handlerFunc,
		Timeout: timeout,
	}
}

func (w *Worker) AttachListener(lf ListenerFunc) {
	w.Listener = lf
}

func (w Worker) Run(stopCh <-chan struct{}) {
	w.Logger.Info().Msg("Started Worker...")
	for {
		job, err := w.DB.NextJob()
		if err != nil {
			w.Logger.Err(err).Msg("error fetching job")
		}

		if job != nil {
			go w.processJob(job, stopCh)
			time.Sleep(1 * time.Second)
		} else {
			w.Logger.Info().Msg("No queued job")
			time.Sleep(3 * time.Second)
		}
	}
}

func (w Worker) processJob(job *model.Job, stopCh <-chan struct{}) {
	ctx, cancel := context.WithTimeout(context.Background(), w.Timeout)
	defer cancel()

	failJob := func(msg string) {
		w.Logger.Info().Msgf("halting job: %s", job.JobId)
		job.EndTime = time.Now()
		job.Status = model.StatusFailed
		job.StatusMessage = msg
		err := w.DB.FailJob(*job)
		if err != nil {
			w.Logger.Err(err).Msgf("could not update failed job: %s", err.Error())
		}
		w.Logger.Info().Msgf("job halted: %+v", job)
	}
	job.StartTime = time.Now()
	err := w.DB.StartJob(*job)

	if err != nil {
		failJob(fmt.Sprintf("could not start job: %s", err.Error()))
	}
	select {
	case <-stopCh:
		failJob(err.Error())
		cancel()
		break
	default:
		w.Logger.Info().Msgf("Processing Job: %s", job.JobId)
		err = w.Handler(ctx, job)
		if err != nil {
			failJob(err.Error())
		} else {
			job.EndTime = time.Now()
			job.Status = model.StatusDone
			w.Logger.Info().Msgf("Completed job: %s", job.JobId)
			// todo: notify API server
			_ = w.DB.MarkJobAsDone(*job)
		}
		if w.Listener != nil {
			err := w.Listener(job)
			if err != nil {
				w.Logger.Err(err).Msg("cannot broadcast job completion")
			}
		}
	}
}
