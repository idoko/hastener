package worker

import (
	"context"
	"fmt"
	"github.com/rs/zerolog"
	"gitlab.com/idoko/hastener/model"
	"os"
	"testing"
	"time"
)

var (
	jobCount = 0
	logger   = zerolog.New(os.Stderr).With().Timestamp().Logger()
)

type fakeDB struct{ sent bool }

func (d *fakeDB) StartJob(job model.Job) (err error) {
	job.StartTime = time.Now()
	return nil
}

func (d *fakeDB) FailJob(job model.Job) (err error) {
	job.Status = model.StatusFailed
	job.StatusMessage = "failed"
	return nil
}

func (d *fakeDB) MarkJobAsDone(job model.Job) (err error) {
	job.Status = model.StatusDone
	job.EndTime = time.Now()
	return nil
}

func (d *fakeDB) NextJob() (j *model.Job, err error) {
	// only send one job
	if !d.sent {
		j = &model.Job{
			ObjectId: int64(jobCount),
			JobId:    fmt.Sprintf("test-%d", jobCount),
		}
		d.sent = true
		return j, nil
	} else {
		return nil, nil
	}
}

func TestWorker_Run(t *testing.T) {
	ddb := fakeDB{}
	jobDuration := 10 * time.Second

	t.Run("stop jobs successfully after 5 seconds", func(t *testing.T) {
		var j *model.Job
		handler := func(ctx context.Context, job *model.Job) error {
			j = job
			slumberTime := 5 * time.Second
			logger.Info().Msgf("Processing job: %s with time %d seconds", job.JobId, slumberTime)
			time.Sleep(slumberTime)
			return nil
		}
		stopCh := make(chan struct{}, 1)
		w := NewWorker(&ddb, logger, jobDuration, handler)
		go w.Run(stopCh)
		// sleep with an extra time to account for lags
		// maybe retry instead of sleeping
		time.Sleep(6 * time.Second)
		if j.Status != model.StatusDone {
			t.Errorf("expected job (%s) to have status %s, got %s", j.JobId, model.StatusDone, j.Status)
			t.Errorf("%v", j)
		}
		// quit the worker
		stopCh <- struct{}{}
	})

	t.Run("job exceeds timeout and fail", func(t *testing.T) {
		ddb := fakeDB{}
		var j *model.Job
		jobDuration := 1 * time.Second
		handler := func(ctx context.Context, job *model.Job) error {
			j = job
			logger.Info().Msgf("Processing job: %s with time", j.JobId)
			select {
			// handler only returns nil after `jobDuration` is passed so it should fail
			case <-time.After(15 * time.Second):
				return nil
			case <-ctx.Done():
				logger.Err(ctx.Err()).Msg("handler stopped...")
				return ctx.Err()
			}
		}

		stopCh := make(chan struct{}, 1)
		w := NewWorker(&ddb, logger, jobDuration, handler)
		go w.Run(stopCh)
		// sleep until job fails (i.e jobDuration has passed)
		time.Sleep(2 * time.Second)
		stopCh <- struct{}{}
		if j.Status != model.StatusFailed {
			t.Errorf("expected job (%s) to fail with status %s and message %s, got %s", j.JobId, model.StatusFailed, j.StatusMessage, j.Status)
		}
	})

	t.Run("job errors out and fail", func(t *testing.T) {
		ddb := fakeDB{}
		var j *model.Job
		jobDuration := 2 * time.Second
		handler := func(ctx context.Context, job *model.Job) error {
			j = job
			return fmt.Errorf("this job always return an error")
		}
		stopCh := make(chan struct{}, 1)
		w := NewWorker(&ddb, logger, jobDuration, handler)
		go w.Run(stopCh)
		time.Sleep(jobDuration)
		if j.Status != model.StatusFailed {
			t.Errorf("expected job to have status: %s with message %s, got %s", model.StatusFailed, j.StatusMessage, j.Status)
		}
		stopCh <- struct{}{}
	})

	t.Run("worker broadcasts job completion to listeners", func(t *testing.T) {
		ddb := fakeDB{}
		var j *model.Job
		handler := func(ctx context.Context, job *model.Job) error {
			j = job
			return nil
		}
		stopCh := make(chan struct{}, 1)
		w := NewWorker(&ddb, logger, jobDuration, handler)
		msg := "job completed and modified by listener"
		w.AttachListener(func(job *model.Job) error {
			job.StatusMessage = msg
			return nil
		})
		go w.Run(stopCh)
		// sleep with an extra time to account for lags
		// maybe retry instead of sleeping because for larger tests this could take longer
		time.Sleep(2 * time.Second)
		if j.Status != model.StatusDone {
			t.Errorf("expected job (%s) to have status %s, got %s", j.JobId, model.StatusDone, j.Status)
		}
		if j.StatusMessage != msg {
			t.Errorf("expected listener to change status message to %s, got %s", msg, j.StatusMessage)
		}
		// quit the worker
		stopCh <- struct{}{}
	})
}
